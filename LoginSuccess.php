<?php
    session_start();
    // var_dump($session);
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // if click button Log out
        if (isset($_POST['logout_action'])) {
            // delete session
            session_destroy();
        }
    }
?>
<html>
<head>
    <title>Login Success</title>
</head>
<body>
    <form method="POST" action="LoginSuccess.php">
        <a href="Login.php"><input type="button" name="logout_action" value="Log out"></a>
        <p>Log out will back to log in page</p>
    </form>
</body>
</html>
<?php

    // exercise 1: create abstract class
    abstract class Supervisor 
    {
        // protected variable
        protected $slogan;

        // getter and setter: get and set value for protected variable
        public function getSlogan()
        {
            return $this->slogan;
        }

        public function setSlogan($sloganValue) 
        {
            $this->slogan = $sloganValue;
        }

        // abstract method
        abstract function saySloganOutLoud();
    }

    // exercise 2: create interface
    interface Boss
    {
        // interface method
        function checkValidSlogan();
    }

    // Bonus 
    trait Active 
    {
        function defindYourSelf() 
        {
            return get_class($this);
        }
    }


    // exercise 3, 4: create and write class Easy and Ugly boss
    class EasyBoss extends Supervisor implements Boss
    {
        use Active;
        /**
         * override method
         *
         */
        public function saySloganOutLoud() 
        {
            echo $this->getSlogan();
        }

        /**
         * override method
         *
         * @return boolean
         */
        public function checkValidSlogan() 
        {
            if (strpos($this->getSlogan(), 'after') !== false || strpos($this->getSlogan(), 'before') !== false) {
                return true;
            }

            return false;
        }
    }

    class UglyBoss extends Supervisor implements Boss
    {   
        use Active;
        /**
         * override method
         *
         */
        public function saySloganOutLoud() 
        {
            echo $this->getSlogan();
        }

        /**
         * override method
         *
         * @return boolean
         */
        public function checkValidSlogan() 
        {
            if (strpos($this->getSlogan(), 'after') !== false && strpos($this->getSlogan(), 'before') !== false) {
                return true;
            }

            return false;            
        }
    }

    // problem: code run expect
    $easyBoss = new EasyBoss();
    $uglyBoss = new UglyBoss();

    $easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

    $uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

    $easyBoss->saySloganOutLoud(); 

    echo "<br>";

    $uglyBoss->saySloganOutLoud(); 

    echo "<br>";

    var_dump($easyBoss->checkValidSlogan()); // true
    echo "<br>";
    var_dump($uglyBoss->checkValidSlogan()); // true
    echo "<br>";

    // Bonus 
    echo 'I am ' . $easyBoss->defindYourSelf(); 
    echo "<br>";
    echo 'I am ' . $uglyBoss->defindYourSelf(); 
?>
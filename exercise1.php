<?php
    function checkValidString($pInput) {		
    //check string 'pInput' return true if valid string, false if invalid string
        //if pInput is empty
        if ($pInput === '') {		
            return true;
        }
        //if the string containing 'after' returns false
        if (strpos($pInput, 'after') !== false)	{
            return false;
        }					
        if (strlen($pInput) > 50 || strpos($pInput, 'before') !== false) {		
        //if length of string > 50 || contain 'before'
            return true;
        } 		
        //if not, return false
        return false;
    }	

    //use the generated function to test the two strings in file1.txt and file2.txt
    $pathFile1 = "file1.txt";		
    //path of file 1
    $pathFile2 = "file2.txt";		
    //path of file 2
    echo "<b>Kiểm tra chuỗi thứ nhất: </b><br>";
    $fp = fopen($pathFile1, "r");		
    //open file
    if (!$fp) {
        echo "Lỗi khi mở file!";		
        //check error
    } else {
        $content = fread($fp, filesize($pathFile1));		
        //read content of file
        if (checkValidString($content)) {				
        //Use the check () function to test the function, return true if valid string
            echo "Chuỗi hợp lệ. <br>";
        } else {		
        //return false if invalid string
            echo "Chuỗi không hợp lệ. <br>";
        }
    }

    //test the second string similar to the first string
    echo "<b>Kiểm tra chuỗi thứ hai: </b><br>";
    $fp = fopen($pathFile2, "r");
    if (!$fp) {
        echo "Lỗi khi mở file!";
    } else {
        $content = fread($fp, filesize($pathFile2));
        if (checkValidString($content)) {
            echo "Chuỗi hợp lệ.";
        } else {
            echo "Chuỗi không hợp lệ.";
        }
    }
?>
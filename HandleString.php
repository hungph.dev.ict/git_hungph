<?php
    class HandleString 
    {

        //property
        public $check1, $check2;

        /**
        * read file
        *
        * @param string $pathName
        * @return false || string
        */
        public function readFile($pathName) {
            $fp = fopen($pathName, "r");
            if (!$fp) {
                echo "Lỗi khi mở file!";
                return false;
            } 

            return fread($fp, filesize($pathName));
        }
        
        /**
         * check valid of string
         *
         * @param string $pString
         * @return boolean
         */
        public function checkValidString($pString) {
            if ($pString === '') {       
                return true;
            }

            if (strlen($pString) > 50 && strpos($pString, 'after') === false) {
                return true;
            }   

            if (strpos($pString, 'after') === false && strpos($pString, 'before') !== false) {      
                return true;
            }  

            return false;
        }    
    }

    // create new object 1
    $object1 = new HandleString();
    if ($object1->readFile('file1.txt') !== false && $object1->readFile('file2.txt') !== false) {
        // assign value for check 1 property
        $object1->check1 = $object1->checkValidString($object1->readFile('file1.txt'));
        // assign value for check 2 property
        $object1->check2 = $object1->checkValidString($object1->readFile('file2.txt'));
    }
?>
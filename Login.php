<?php
    // start session
    session_start();
?>
<html>
<head>
    <meta charset="utf-8">
	<title>PHP web</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
        /**
         * check valid email
         *
         * @param email
         * @return boolean
         */
        function valid_email($email)
        {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }

        /**
         * check length of email
         *
         * @param email
         * @return boolean
         */
        function length_email($email)
        {
            return strlen(($email) <= 256);
        } 

        /**
         * check length of password
         *
         * @param password
         * @return boolean
         */
        function length_password($password)
        {
            return strlen($password) >= 6 && strlen($password) <= 50;
        } 

        $error = array();
        $data = array();

        // if click button Log in
        if (isset($_POST['login_action'])) {
            // get data from method post
            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
            $data['remember_me'] = isset($_POST['remember_me']) ? $_POST['remember_me'] : '';

            // validate email
            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!valid_email($data['email']) && !length_email($data['email'])) {
                $error['email'] = 'Email không đúng định dạng và độ dài vượt quá 256 ký tự'; 
            } elseif (!valid_email($data['email']) && length_email($data['email'])) {
                $error['email'] = 'Email không đúng định dạng';
            } elseif (valid_email($data['email']) && !length_email($data['email'])) {
                $error['email'] = 'Email vượt quá 256 ký tự';
            }

            // validate password
            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập password';
            } elseif (!length_password($data['password'])) {
                $error['password'] = 'Password chỉ cho phép độ dài từ 6-50 ký tự';   
            }

            // save data 
            if (!$error) {
                echo '<h1>Đăng nhập thành công</h1><hr>';
                $_SESSION['email'] = $data['email'];
                // set cookie
                if($data['remember_me'] == 'on')
                {
                    $hour = time() + 3600 * 24 * 30;
                    setcookie('email', $data['email'], $hour);
                    setcookie('password', $data['password'], $hour);
                }
            } else {
                echo '<h1>Đăng nhập thất bại</h1><hr>';
            }
        }
    ?>
    <h2>Input Form</h2>
    <form method="POST" action="Login.php">
        <table cellspacing="0" cellpadding="5">
            <tr>
                <td>Email</td>
                <td>
                    <input type="mail" name="email" id="email" value="<?php if(isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>"/>
                    <?php echo isset($error['email']) ? $error['email'] : ''; ?>
                </td>
            </tr>	
            <tr>
                <td>Password</td>
                <td>
                    <input type="password" name="password" id="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>"/>
                    <?php echo isset($error['password']) ? $error['password'] : '';?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="checkbox" name="remember_me" id="remember_me">Remember me</td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="login_action" value="Log in"/></td>
            </tr>
            <tr>
            <?php echo (!$error) ? '<a href="LoginSuccess.php">Click here go to login success page</a>' : '';?>
            </tr>
        </table>
    </form>
</body>
</html>
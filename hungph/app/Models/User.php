<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'users';
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    public static $role = [
        self::ROLE_ADMIN => "Quản trị viên",
        self::ROLE_STAFF => "Nhân viên"
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mail_address', 'password', 'phone', 'address', 'role', 'classroom_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the classroom for the user.
     */
    public function classroom()
    {
        return $this->belongsTo('App\Models\Classroom');
    }

    /**
     * Scope a query to search users follow mail address.
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMailAddress($query, $mailAddress)
    {
        return $query->where('mail_address', 'LIKE', '%' . $mailAddress . '%');
    }

    /**
     * Scope a query to search users follow name.
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', 'LIKE', '%' . $name . '%');
    }

    /**
     * Scope a query to search users follow address.
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAddress($query, $address)
    {
        return $query->where('address', 'LIKE', '%' . $address . '%');
    }

    /**
     * Scope a query to search users follow classroom.
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClassroom($query, $classroomId)
    {
        return $query->with('classroom')->where('classroom_id', $classroomId);
    }

    /**
     * Scope a query to search users follow phone.
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopePhone($query, $phone)
    {
        return $query->where('phone', $phone);
    }

    /**
     * Create new user in table Users 
     *
     * @param array $data
     * @return 
     */
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }

    /**
     * Get all user in table Users and find user with condition
     *
     * @param 
     * @return $users
     */
    public function getUser(array $data)
    {
        $builder = User::orderBy('mail_address', 'ASC');    

        if (isset($data['mail_address']) && $data['mail_address'] != '') {
            $builder->mailAddress($data['mail_address']);
        }

        if (isset($data['name']) && $data['name'] != '') {
            $builder->name($data['name']);
        }
        if (isset($data['address']) && $data['address'] != '') {
            $builder->address($data['address']);
        }

        if (isset($data['classroom_id']) && $data['classroom_id'] != '') {
            $builder->classroom($data['classroom_id']);
        }

        if (isset($data['phone']) && $data['phone'] != '') {
            $builder->phone($data['phone']);
        }

        return $builder->paginate(20);
    }
}
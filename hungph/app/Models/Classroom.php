<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    /**
     * Get the users for the classroom.
     */
    public function users()
    {
        return $this->hasMany('App\Models\Users');
    }

    /**
     * Get all classroom in table Classrooms
     *
     * @param 
     * @return $classrooms
     */
    public function getClassroom()
    {
        return Classroom::pluck('name', 'id');
    }
}
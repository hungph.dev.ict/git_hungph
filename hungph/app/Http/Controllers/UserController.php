<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use App\Models\Classroom;
use App\Events\CreatedUser;

use Helper;

class UserController extends Controller
{

    /**
     * The user implementation.
     *
     * @var User
     */
    protected $user;

    protected $classroom;

    /**
     * Create a new controller instance.
     *
     * @param User $users
     * @return void
     */
    public function __construct(User $user, Classroom $classroom)
    {
        $this->user = $user;
        $this->middleware([
            'auth', 
            'classable',
        ]);
        $this->classroom = $classroom;
    }

    /**
     * Show all users, return view a list of users
     *
     * @param 
     * @return view users.index
     */
    public function index(Request $request) 
    {
        $classrooms = $this->classroom->getClassroom();

        $this->authorize('staff');
        $data = $request->all();
        $users = $this->user->getUser($data);
        return view('users.index', compact(
            'users',
            'classrooms'
        ));
    }

    /**
     * Redirect to view create an user
     *
     * @param 
     * @return view users.create
     */
    public function create() 
    {
        $classrooms = $this->classroom->getClassroom();

        $this->authorize('admin');

    	return view('users.create', compact(
            'classrooms'
        ));
    }

    /**
     * Store user from request and 
     *
     * @param Request $request
     * @return view users.index
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $user= $this->user->createUser($data);
        event(new CreatedUser($user));
        flash('Thêm mới người dùng thành công')->success();
        return redirect()->route('users.index');
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HelloWorldController extends Controller
{
    /**
     * Show the view for the given user.
     *
     * @param null
     * @return view
     */
    public function show()
    {
        return view('hello_world.show');
    }
}
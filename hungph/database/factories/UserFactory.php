<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'mail_address' => $faker->email,
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->e164phoneNumber,
        'password' => Hash::make($faker->password), // secret
        'role' => rand(App\Models\User::ROLE_ADMIN, App\Models\User::ROLE_STAFF),
        'classroom_id' => App\Models\Classroom::all()->random()->id,
    ];
});
<?php

use Illuminate\Database\Seeder;

class ClassroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('classrooms')->truncate();
        Schema::enableForeignKeyConstraints();

        factory(App\Models\Classroom::class, 10)->create();
    }
}
